fn matches(n: i32) -> bool {
    let s = n.to_string();

    for first in 0..=4 {
        if s.chars().nth(first) > s.chars().nth(first + 1) {
            return false;
        }
    }

    for first in 0..=4 {
        if s.chars().nth(first) == s.chars().nth(first + 1) {
            return true;
        }
    }

    false
}

fn main() {
    let mut num_match = 0;
    for n in 171309..=643603 {
        if matches(n) {
            num_match += 1;
        }
    }

    println!("{}", num_match);
}
