use std::io::{self};
use std::collections::HashSet;

#[derive(Debug)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

#[derive(Debug)]
struct Instr {
    direction: Direction,
    distance: i32,
}

fn parse_direction(s: &str) -> Vec<Instr> {
    let mut ret: Vec<Instr> = Vec::new();
    let raw: Vec<&str> = s.trim().split_terminator(',').collect();

    for r in raw {
        let dir_raw = r.chars().nth(0).unwrap();
        let dir = match dir_raw {
            'U' => Direction::Up,
            'R' => Direction::Right,
            'D' => Direction::Down,
            'L' => Direction::Left,
            _ => panic!("wut")
        };
        let num = &r[1..];
        let num: i32 = num.parse::<i32>().unwrap();

        let instr = Instr {
            distance: num,
            direction: dir,
        };
        ret.push(instr);
    }
    
    return ret;
}

fn visited_points(instructions: &Vec<Instr>) -> HashSet<(i32, i32)> {
    let mut cur = (0, 0);
    let mut visited: HashSet<(i32, i32)> = HashSet::new();

    for instr in instructions {
        match &instr.direction {
            Direction::Up => {
                for _ in 0..instr.distance {
                    cur.1 += 1;
                    println!("{:?}", cur);
                    visited.insert(cur);
                }
            }
            Direction::Down => {
                for _ in 0..instr.distance {
                    cur.1 -= 1;
                    println!("{:?}", cur);
                    visited.insert(cur);
                }
            }
            Direction::Left => {
                for _ in 0..instr.distance {
                    cur.0 -= 1;
                    println!("{:?}", cur);
                    visited.insert(cur);
                }
            }
            Direction::Right => {
                for _ in 0..instr.distance {
                    cur.0 += 1;
                    println!("{:?}", cur);
                    visited.insert(cur);
                }
            }
        }
    }

    visited
}

fn main() -> io::Result<()> {
    let stdin = io::stdin();

    let mut path1 = String::new();
    let mut path2 = String::new();
    stdin.read_line(&mut path1).unwrap();
    stdin.read_line(&mut path2).unwrap();

    let instr1 = parse_direction(&path1);
    let instr2 = parse_direction(&path2);
    println!("Visit 1");
    let visited1 = visited_points(&instr1);
    println!("Visit 2");
    let visited2 = visited_points(&instr2);
    //println!("{:?}", visited1);
    //println!("{:?}", visited2);
    let inter = visited1.intersection(&visited2);
    println!("{:?}", inter);

    for i in inter {
        println!("{}", i.1 + i.0);
    }

    Ok(())
}