use std::io::{self, Read};

const WIDTH: usize = 25;
const HEIGHT: usize = 6;
const IMGSIZE: usize = WIDTH * HEIGHT;

fn count_chr(layer: &[u8], chr: u8) -> usize {
    let mut numzeros = 0;

    for c in layer {
        if *c == chr {
            numzeros += 1;
        }
    }

    numzeros
}

#[derive(Debug)]
struct Layer {
    data: Vec<u8>,
    numzeros: usize,
    numones: usize,
    numtwos: usize,
}

fn main() -> io::Result<()> {
    let mut buffer: Vec<u8> = Vec::new();
    io::stdin().read_to_end(&mut buffer)?;
    buffer.pop();

    //println!("len = {}", buffer.len());

    let numlayers = buffer.len() / (WIDTH * HEIGHT);
    let mut n = 0;
    let mut layers: Vec<Layer> = Vec::new();
    while n < numlayers {
        let start = n * IMGSIZE;
        let end = start + IMGSIZE;
        let img: &[u8] = &buffer[start..end];

        //println!("layer {} = {:?}", n, img);
        n += 1;
        let numzeros = count_chr(&img, '0' as u8);
        let numones = count_chr(&img, '1' as u8);
        let numtwos = count_chr(&img, '2' as u8);
        //println!("numzeros = {}", numzeros);

        let layer = Layer {
            data: Vec::from(img),
            numzeros,
            numones,
            numtwos,
        };

        layers.push(layer);
    }

    layers.sort_unstable_by_key(|l| l.numzeros);

    let first: &Layer = layers.first().unwrap();
    println!("{}", first.numones * first.numtwos);

    Ok(())
}
