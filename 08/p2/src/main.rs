use std::io::{self, Read};

const WIDTH: usize = 25;
const HEIGHT: usize = 6;
const IMGSIZE: usize = WIDTH * HEIGHT;

#[derive(Debug)]
struct Layer {
    data: Vec<u8>,
}

fn main() -> io::Result<()> {
    let mut buffer: Vec<u8> = Vec::new();
    io::stdin().read_to_end(&mut buffer)?;
    buffer.pop();

    //println!("len = {}", buffer.len());

    let numlayers = buffer.len() / (WIDTH * HEIGHT);
    let mut n = 0;
    let mut layers: Vec<Layer> = Vec::new();
    while n < numlayers {
        let start = n * IMGSIZE;
        let end = start + IMGSIZE;
        let img: &[u8] = &buffer[start..end];

        n += 1;

        let layer = Layer {
            data: Vec::from(img),
        };

        layers.push(layer);
    }

    let mut image: Vec<u8> = vec!['2' as u8; IMGSIZE];

    for l in layers {
        for i in 0..150 {
            if image[i] == '2' as u8 {
                
                image[i] = l.data[i];
            }
        }
    }

    for row in 0..HEIGHT {
        for col in 0..WIDTH {
            let px = image.get(row * WIDTH + col).unwrap();
            if *px == '0' as u8 {
                print!("{}", 'o');
            } else if *px == '1' as u8 {
                print!("{}", ' ');
            } else {
                print!("{}", '?');
            }
        }
        println!("");
    }

    Ok(())
}
