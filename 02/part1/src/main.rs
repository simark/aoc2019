use std::fs;

fn main() {
    // 1  add
    // 2  mul
    // 99 end
    let content = fs::read_to_string("./input").unwrap();
    let content : Vec<&str> = content.split_terminator(',').collect();
    let mut content : Vec<u32> = content.iter().map(|x| x.trim().parse().unwrap()).collect();
    let mut done = false;
    let mut pc = 0;

    content[1] = 12;
    content[2] = 2;

    while !done {
        let opcode = content[pc];
        match opcode {
            1 => {
                println!("Add");
                let src1 = content[pc + 1] as usize;
                let src2 = content[pc + 2] as usize;
                let dst = content[pc + 3] as usize;
                content[dst] = content[src1] + content[src2];
                pc += 4;
            }
            2 => {
                println!("Mul");
                let src1 = content[pc + 1] as usize;
                let src2 = content[pc + 2] as usize;
                let dst = content[pc + 3] as usize;
                content[dst] = content[src1] * content[src2];
                pc += 4;
            }
            99 => {
                println!("Quit");
                done = true;
            }
            _ => ()
        }
    }

    println!("{:?}", content);
}