const INITIAL_STATE : [u32; 149] = [1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 1, 10, 19, 1, 19, 5, 23, 2, 23, 6, 27, 1, 27, 5, 31, 2, 6, 31, 35, 1, 5, 35, 39, 2, 39, 9, 43, 1, 43, 5, 47, 1, 10, 47, 51, 1, 51, 6, 55, 1, 55, 10, 59, 1, 59, 6, 63, 2, 13, 63, 67, 1, 9, 67, 71, 2, 6, 71, 75, 1, 5, 75, 79, 1, 9, 79, 83, 2, 6, 83, 87, 1, 5, 87, 91, 2, 6, 91, 95, 2, 95, 9, 99, 1, 99, 6, 103, 1, 103, 13, 107, 2, 13, 107, 111, 2, 111, 10, 115, 1, 115, 6, 119, 1, 6, 119, 123, 2, 6, 123, 127, 1, 127, 5, 131, 2, 131, 6, 135, 1, 135, 2, 139, 1, 139, 9, 0, 99, 2, 14, 0, 0];

fn run_simulation(noun: u32, verb: u32) -> u32 {
    // 1  add
    // 2  mul
    // 99 end
    let mut content = INITIAL_STATE.to_vec();
    println!("{:?}", content);
    let mut done = false;
    let mut pc = 0;

    content[1] = noun;
    content[2] = verb;

    while !done {
        let opcode = content[pc];
        match opcode {
            1 => {
                println!("Add");
                let src1 = content[pc + 1] as usize;
                let src2 = content[pc + 2] as usize;
                let dst = content[pc + 3] as usize;
                content[dst] = content[src1] + content[src2];
                pc += 4;
            }
            2 => {
                println!("Mul");
                let src1 = content[pc + 1] as usize;
                let src2 = content[pc + 2] as usize;
                let dst = content[pc + 3] as usize;
                content[dst] = content[src1] * content[src2];
                pc += 4;
            }
            99 => {
                println!("Quit");
                done = true;
            }
            _ => ()
        }
    }

    content[0]
}

fn main() {
    'yo: for noun in 0..100 {
        for verb in 0..100 {
            let res = run_simulation(noun, verb);
            if res == 19690720 {
                println!("{} {} {}", noun, verb, 100*noun + verb);
                break 'yo;
            }
        }
    }
}