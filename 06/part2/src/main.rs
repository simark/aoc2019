use std::io::{self, Read};
use std::collections::HashMap;
use std::collections::HashSet;

fn visited_nodes_until_com(parents: &HashMap<String, String>, obj: &str)
        -> HashSet<String> {
    let mut ret: HashSet<String> = HashSet::new();
    let mut obj = obj;
    while obj != "COM" {
        obj = parents.get(obj).unwrap();
        ret.insert(String::from(obj));
    }

    ret
}

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;

    let cx: Vec<&str> = buffer.split_terminator('\n').collect();

    let mut parents: HashMap<String, String> = HashMap::new();

    for c in cx {
        let parent = &c[0..=2];
        let child = &c[4..=6];
        parents.insert(String::from(child), String::from(parent));
    }

    let my_route = visited_nodes_until_com(&parents, "YOU");
    let san_route = visited_nodes_until_com(&parents, "SAN");
    let between: HashSet<&String> =
        my_route.symmetric_difference(&san_route).collect();

    println!("{:?}", between.len() + 1);

    Ok(())
}