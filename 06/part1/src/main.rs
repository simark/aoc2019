use std::io::{self, Read};
use std::collections::HashMap;

fn num_orbits_of(parents: &HashMap<String, String>, obj: &str)
        -> u32 {
    let mut n: u32 = 0;
    let mut obj = obj;
    while obj != "COM" {
        obj = parents.get(obj).unwrap();
        n = n + 1;
    }

    n
}

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;

    let cx: Vec<&str> = buffer.split_terminator('\n').collect();

    let mut parents: HashMap<String, String> = HashMap::new();

    for c in cx {
        let parent = &c[0..=2];
        let child = &c[4..=6];
        parents.insert(String::from(child), String::from(parent));
    }

    let mut total = 0;
    for obj in parents.keys() {
        let n = num_orbits_of(&parents, obj);
        total += n;
    }

    println!("{:?}", total);

    Ok(())
}