import bt2
import uuid
from computer import Computer

resultats = []
queue = [0]

def _create_trace_class(self_comp):
    tc = self_comp._create_trace_class()
    sc = tc.create_stream_class()
    payload = tc.create_structure_field_class()
    payload.append_member('val',
        tc.create_signed_integer_field_class(field_value_range=64))
    ec = sc.create_event_class(name='hello', payload_field_class=payload)
    return tc, sc, ec

class HelloSourceIter(bt2._UserMessageIterator):
    def __init__(self, config, port):
        tc, sc, self._ec = port.user_data

        self._trace = tc()
        self._stream = self._trace.create_stream(sc)

    def __next__(self):
        val = queue.pop(0)

        ev_msg = self._create_event_message(self._ec, self._stream)
        ev_msg.event.payload_field['val'] = val

        return ev_msg


class HelloSource(bt2._UserSourceComponent, message_iterator_class=HelloSourceIter):
    def __init__(self, config, params, obj):
        tc, sc, ec = _create_trace_class(self)

        self._add_output_port("out", user_data=(tc, sc, ec))


class ComputerFilterIter(bt2._UserMessageIterator):
    def __init__(self, config, port):
        mem = port.user_data['mem'].copy()
        self._computer = Computer(mem, self._input_func, self._output_func)
        self._phase = port.user_data['phase']
        self._tc, self._sc, self._ec = port.user_data['classes']
        self._trace = self._tc()
        self._stream = self._trace.create_stream(self._sc)
        self._output = None
        self._upstream_iter = self._create_input_port_message_iterator(self._component._input_ports['in'])

    def _input_func(self):
        print('INPUT FUNC')
        if self._phase is not None:
            print('RETURNING PHASE', self._phase)
            phase = self._phase
            self._phase = None
            return phase
        
        print('GETTING FROM UPSTREAM')
        while True:
            msg = next(self._upstream_iter)
            if type(msg) == bt2._StreamBeginningMessageConst:
                pass
            elif type(msg) == bt2._EventMessageConst:
                return int(msg.event.payload_field['val'])
            else:
                assert False

    def _output_func(self, val):
        print('OUTPUT FUNC', val)
        self._output = val

    def __next__(self):
        while True:
            cont = self._computer.step()
            if not cont:
                raise StopIteration
            
            if self._output is not None:
                ev_msg = self._create_event_message(self._ec, self._stream)
                ev_msg.event.payload_field['val'] = self._output
                self._output = None
                return ev_msg


class ComputerFilter(bt2._UserFilterComponent,
        message_iterator_class=ComputerFilterIter):
    def __init__(self, config, params, obj):
        self._add_input_port("in")
        mem = mem_from_string(str(params['mem']))
        tc, sc, ec = _create_trace_class(self)
        self._add_output_port("out", user_data={
            'mem': mem,
            'phase': int(params['phase']),
            'classes': (tc, sc, ec),
        })


class HelloSink(bt2._UserSinkComponent):
    def __init__(self, config, params, obj):
        self._add_input_port("in")

    def _user_graph_is_configured(self):
        self._iter = self._create_input_port_message_iterator(self._input_ports["in"])

    def _user_consume(self):
        msg = next(self._iter)
        print(msg)
        if type(msg) == bt2._EventMessageConst:
            print(msg.event.payload_field['val'], 'OUTPUT DANS LE SINK')
            queue.append(int(msg.event.payload_field['val']))
            resultats.append(int(msg.event.payload_field['val']))
        else:
            assert False


def mem_from_string(s):
    return [int(x) for x in s.split(',')]

mem = "3,8,1001,8,10,8,105,1,0,0,21,38,63,76,89,106,187,268,349,430,99999,3,9,1001,9,5,9,102,3,9,9,1001,9,2,9,4,9,99,3,9,101,4,9,9,102,3,9,9,101,4,9,9,1002,9,3,9,101,2,9,9,4,9,99,3,9,101,5,9,9,1002,9,4,9,4,9,99,3,9,101,2,9,9,1002,9,5,9,4,9,99,3,9,1001,9,5,9,1002,9,5,9,1001,9,5,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,99"
#mem = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"

def run(phases):
    global queue
    queue = [0]
    g = bt2.Graph()

    start = g.add_component(HelloSource, "start")
    a = g.add_component(ComputerFilter, 'a', params={"mem": mem, 'phase': phases[0]})
    b = g.add_component(ComputerFilter, 'b', params={"mem": mem, 'phase': phases[1]})
    c = g.add_component(ComputerFilter, 'c', params={"mem": mem, 'phase': phases[2]})
    d = g.add_component(ComputerFilter, 'd', params={"mem": mem, 'phase': phases[3]})
    e = g.add_component(ComputerFilter, 'e', params={"mem": mem, 'phase': phases[4]})
    end = g.add_component(HelloSink, "end")

    g.connect_ports(start.output_ports["out"], a.input_ports["in"])
    g.connect_ports(a.output_ports["out"], b.input_ports["in"])
    g.connect_ports(b.output_ports["out"], c.input_ports["in"])
    g.connect_ports(c.output_ports["out"], d.input_ports["in"])
    g.connect_ports(d.output_ports["out"], e.input_ports["in"])
    g.connect_ports(e.output_ports["out"], end.input_ports["in"])

    g.run()

run([5,7,6,9,8])

import itertools
for x in itertools.permutations([5,6,7,8,9]):
    print('CONFIG', x)
    run(x)

print('MAX', max(resultats))
