class Computer:
    def __init__(self, mem, input_func, output_func):
        self._mem = mem
        self._pc = 0
        self._if = input_func
        self._of = output_func

    def _operand(self, mode, src):
        if mode == 1:
            return src
        elif mode == 0:
            return self._mem[src]
        else:
            raise Exception("boo")

    def _binop_operands(self, mode1, mode2):
        src1 = self._mem[self._pc + 1]
        src1 = self._operand(mode1, src1)
        src2 = self._mem[self._pc + 2]
        src2 = self._operand(mode2, src2)
        dst = self._mem[self._pc + 3]
        return src1, src2, dst

    def step(self):
        full_op = self._mem[self._pc]

        op = full_op % 100
        mode1 = (full_op // 100) % 10
        mode2 = (full_op // 1000) % 10
        mode3 = (full_op // 10000) % 10

        print("pc: {} full_op: {}".format(self._pc, full_op))

        if op == 1:
            src1, src2, dst = self._binop_operands(mode1, mode2)
            print("ADD {} + {} -> {}".format(src1, src2, dst))
            self._mem[dst] = src1 + src2
            self._pc += 4
        elif op == 2:
            src1, src2, dst = self._binop_operands(mode1, mode2)
            self._mem[dst] = src1 * src2
            self._pc += 4
        elif op == 3:
            print("INPUT")
            inp = self._if()
            assert type(inp) == int
            dst = self._mem[self._pc + 1]
            self._mem[dst] = inp
            self._pc += 2
        elif op == 4:
            val = self._operand(mode1, self._mem[self._pc + 1])
            print("OUT {}".format(val))
            self._of(val)
            self._pc += 2
        elif op == 5:
            cond = self._operand(mode1, self._mem[self._pc + 1])
            if cond != 0:
                self._pc = self._operand(mode2, self._mem[self._pc + 2])
            else:
                self._pc += 3
        elif op == 6:
            cond = self._operand(mode1, self._mem[self._pc + 1])
            if cond == 0:
                self._pc = self._operand(mode2, self._mem[self._pc + 2])
            else:
                self._pc += 3
        elif op == 7:
            src1, src2, dst = self._binop_operands(mode1, mode2)
            self._mem[dst] = 1 if src1 < src2 else 0
            self._pc += 4
        elif op == 8:
            src1, src2, dst = self._binop_operands(mode1, mode2)
            self._mem[dst] = 1 if src1 == src2 else 0
            self._pc += 4
        elif op == 99:
            print("END")
            return False
        else:
            raise Exception("Unhandled opcode {}".format(op))

        return True


def read_mem_file(f):
    return [int(x.strip()) for x in f.read().strip().split(",") if len(x) > 0]


if __name__ == "__main__":
    with open("input") as f:
        mem = read_mem_file(f)

    def input_func():
        return 5

    def output_func(val):
        print("output: {}".format(val))

    c = Computer(mem, input_func, output_func)

    while c.step():
        pass
