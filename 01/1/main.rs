use std::fs;

fn main() {
    let content = fs::read_to_string("./input");
    let content = content.unwrap();
    let lines = content.split_terminator('\n');
    let mut sum = 0;
    for line in lines {
        let mass: u32 = line.parse().unwrap();
        let fuel = (mass / 3) - 2;
        sum += fuel;
    }

    println!("{}", sum);
}
