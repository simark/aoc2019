use std::fs;

fn main() {
    let content = fs::read_to_string("./input");
    let content = content.unwrap();
    let lines = content.split_terminator('\n');
    let mut sum = 0;
    for line in lines {
        let mass: i32 = line.parse().unwrap();
        let mut fuel: i32 = (mass / 3) - 2;

        while fuel > 0 {
            sum += fuel;
            fuel = (fuel / 3) - 2;
        }
    }

    println!("{}", sum);
}
