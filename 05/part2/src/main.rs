use std::io::{self};
use std::fs;

fn mem_or_immediate(mem: &Vec<i32>, mode: u32, src: i32) -> i32 {
    if mode == 1 {
        src
    } else {
        mem[src as usize]
    }
}

fn binop_operands(pc: usize, mem: &Vec<i32>, mode1: u32, mode2: u32) -> (i32, i32, usize) {
    let src1 = mem[pc + 1];
    let src2 = mem[pc + 2];
    let dst = mem[pc + 3] as usize;
    let val1: i32 = mem_or_immediate(&mem, mode1, src1);
    let val2: i32 = mem_or_immediate(&mem, mode2, src2);

    (val1, val2, dst)
}

fn execute(mem: &mut Vec<i32>) {
    // 1  add
    // 2  mul
    // 99 end
    let mut done = false;
    let mut pc: usize = 0;

    while !done {
        let mut opcode_all = mem[pc] as u32;
        println!("opcode at {} is {}", pc, opcode_all);
        let opcode = opcode_all % 100;
        opcode_all /= 100;
        let mode1 = opcode_all % 10;
        opcode_all /= 10;
        let mode2 = opcode_all % 10;
        opcode_all /= 10;
        let _mode3 = opcode_all % 10;
        match opcode {
            1 => {
                let (val1, val2, dst) = binop_operands(pc, &mem, mode1, mode2);
                println!("Add {} {} {}", val1, val2, dst);
                mem[dst] = val1 + val2;
                pc += 4;
            }
            2 => {
                let (val1, val2, dst) = binop_operands(pc, &mem, mode1, mode2);
                println!("Mul {} {} {}", val1, val2, dst);
                mem[dst] = val1 * val2;
                pc += 4;
            }
            3 => {
                println!("plz input");
                let mut buf = String::new();
                io::stdin().read_line(&mut buf).unwrap();
                let val = buf.trim().parse::<i32>().unwrap();
                let dst = mem[pc + 1] as usize;
                mem[dst] = val;
                pc += 2;
            }
            4 => {
                let src = mem[pc + 1];
                let val = mem_or_immediate(&mem, mode1, src);
                println!("OUTPUT {}", val);
                pc += 2;
            }
            5 => {
                // jump if true
                let src = mem_or_immediate(&mem, mode1, mem[pc + 1]);
                let jump_dest = mem_or_immediate(&mem, mode2, mem[pc + 2]);
                println!("Jump if true {} {}", src, jump_dest);
                if src != 0 {
                    pc = jump_dest as usize;
                } else {
                    pc += 3
                }
            }
            6 => {
                // jump if false
                let src = mem_or_immediate(&mem, mode1, mem[pc + 1]);
                let jump_dest = mem_or_immediate(&mem, mode2, mem[pc + 2]);
                println!("Jump if false {} {}", src, jump_dest);
                if src == 0 {
                    pc = jump_dest as usize;
                } else {
                    pc += 3
                }
            }
            7 => {
                let (val1, val2, dst) = binop_operands(pc, &mem, mode1, mode2);
                println!("Less {} {} {}", val1, val2, dst);
                mem[dst] = { if val1 < val2 { 1 } else { 0 } };
                pc += 4;
            }
            8 => {
                let (val1, val2, dst) = binop_operands(pc, &mem, mode1, mode2);
                println!("Eq {} {} {}", val1, val2, dst);
                mem[dst] = { if val1 == val2 { 1 } else { 0 } };
                pc += 4;
            }
            99 => {
                println!("Quit");
                done = true;
            }
            _ => panic!("Unknown opcode {}", opcode)
        }
    }
}

fn main() -> io::Result<()> {
    let data = fs::read_to_string("input").unwrap();

    let mem: Vec<&str> = data.trim().split_terminator(',').collect();
    let mut mem: Vec<i32> = mem.iter().map(|x| x.parse::<i32>().unwrap()).collect();

    execute(&mut mem);

    Ok(())
}